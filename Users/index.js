import React from 'react';
import { withRouter } from 'react-router';
import { Table } from 'react-bootstrap'

class Users extends React.Component {

    state = {
        users: []
    }
    componentDidMount() {
        this.fetchUsers()
    }

    fetchUsers = (UserName, password) => {
        const API_HEADERS_AND_MODE = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST'
            },
            mode: 'cors',
        };

        const API_SETTINGS = {
            settings: {
                method: 'GET'
            }
        }

        return fetch('http://localhost:5000/users', {
            ...API_HEADERS_AND_MODE,
            ...API_SETTINGS.settings
        }).then(res => {
            if (res.status >= 400) {
                return res.json().then(err => {
                    throw err;
                });
            }
            console.log(res)
            return res.json();
        }).then(data => {
            const newData = data.map(user => {
                let newUser = {}
                newUser.key = user[0]
                newUser.name = user[1]
                newUser.password = user[2]
                newUser.role = 'trader'
                return newUser
            })
            this.setState({ users: newData })
        })
    }

    render() {
        console.log(this.state)
        return (
            <div>
                <Table striped condensed hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Password</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                
                            {this.state.users.map(this.renderPerson)}
            
                    </tbody>
                </Table>
            </div>
        )
    }

    renderPerson(person, index) {
        return (
            <tr key={index}>
                <th>{person.name}</th>
                <th>{person.password}</th>
                <th>{person.role}</th>
            </tr>
        )
    }
};

export default withRouter(Users);
