import React from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Header } from '../Header/index';


export default class CreateUserForm extends React.Component {
  state = {
    UserName: '',
    password: '',
  }

  onChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  }

  
  sendUserCreationRequest = (UserName, password) => {
    const { router } = this.props;
    const API_HEADERS_AND_MODE = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST'
      },
      mode: 'cors',
    };

    const API_SETTINGS = {
      settings: {
        method: 'POST',
        body: JSON.stringify({
          username: UserName,
          password: password,
          role: 'trader'
        })
      }
    }

    return fetch('http://localhost:5000/user', {
      ...API_HEADERS_AND_MODE,
      ...API_SETTINGS.settings
    }).then(res => {
      if (res.status >= 400) {
        return res.json().then(err => {
          throw err;
        });
      }
      console.log(res)
      return res.json();
    }).then(data => {
      router.push('/success')
    })
  }


  render() {
    const { UserName, password } = this.state;

    return (
      <div>
        <Container>
          <Row>
          <Col>
              <Form>
                <Form.Text className="text-muted">
                  Create User
                </Form.Text>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>User Name</Form.Label>
                  <Form.Control id="UserName" onChange={this.onChange} type="input" placeholder="Enter user name" />
                  <Form.Text className="text-muted">
                    We'll never share your UserName with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control  id="password" onChange={this.onChange} type="input" placeholder="Password" />
                </Form.Group>
                <Button variant="primary" onClick={() => this.sendUserCreationRequest(UserName, password)}>
                  Create user
               </Button>
              </Form>
            </Col>

            </Row>
        </Container>
      </div>
    )
  }
}

