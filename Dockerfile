from nginx:alpine
run npm install
add * /
expose 80
cmd ["nginx" , "-g", "daemon off;"]