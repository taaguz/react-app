import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import Login from './Login';
import Success from './Success';
import { requiredAuth } from './authentication';
import style from 'bootstrap/dist/css/bootstrap.css';
import CreateUserForm from './CreateUserForm/index';
import RegistrationForm from './RegistrationForm';

render(
  <Router history={browserHistory}>
      <Route path="/" exact component={Login} />
      <Route path="/success" component={Success} onEnter={requiredAuth}/>
      <Route path="/createuser" component={CreateUserForm} onEnter={requiredAuth}/>
      <Route path="/updateuser" component={RegistrationForm} onEnter={requiredAuth}/>
  </Router>, document.getElementById('app')
)
