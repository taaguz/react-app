import React from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { logOut } from '../authentication';

export default class Header extends React.Component {
    render(props) {
        let links = [];
        if (this.props.role == 'security') {
            links = [
                {
                    link: "/createuser",
                    name: "Create User",
                    key: 1
                },
                {
                    link: "/updateuser",
                    name: "Update User",
                    key: 2
                }
            ]
        } 

        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="/">Deal DB</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {
                            links.map(link => (<Nav.Link href={link.link}>{link.name}</Nav.Link>))
                        }
                    </Nav>
                    <Form inline>
                        <Navbar.Collapse className="justify-content-end">
                            <Navbar.Text>
                                Signed in as: <b>{this.props.role}</b>
                            </Navbar.Text>
                        </Navbar.Collapse>

                        <Button variant="outline-success" onClick={logOut}>Log Out</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}
