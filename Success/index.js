import React from 'react';
import { getToken } from '../authentication';
import './Success.css';
import { sendLoginInformation } from '../Login';
import Header from '../Header/index';
import RegistrationForm from '../RegistrationForm/index';
import Users from '../Users/index';

class Success extends React.Component {
  state = {
    role: '',
    status: '',
    authModal: false,
    secondsElapsed: 0,
    email: '',
    password: '',
  }

  componentDidMount() {
    this.validateTokenViaGet();
    this.interval = setInterval(() => this.tick(), 10000)
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick() {
    this.setState((prevState) => ({
      secondsElapsed: prevState.secondsElapsed + 1
    }));
    if(this.state.secondsElapsed % 10 == 0 && !this.props.authModal) {
      this.validateTokenViaGet();
    }
  }

  onChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  }

  validateTokenViaGet() {
    const API_HEADERS_AND_MODE = {
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST'
      },
      mode: 'cors',
    };

    const API_SETTINGS = {
      settings: {
        method: 'GET'
      }
    };

    return fetch('http://localhost:5000/test_get_with_validation', {
      ...API_HEADERS_AND_MODE,
      ...API_SETTINGS
    }).then(res => {
      if (res.status >= 400) {
        this.setState({
          authModal: true
        });
        return res.json().then(err => {
          throw err;
        });
      }
      this.setState({
        authModal: false,
      });
      return res.json();
    }).then(data => {
      console.log(data)
      this.setState({
        status: data.status,
        role: data.role,
      });
    })
  }

  render() {
    const token = getToken();
    let page;
    if (this.state.role === 'security') {
      page =  <Users/>;
    } else {
      page = <div>trader</div>;
    }
    return (
      <div>
        <Header role={this.state.role} />
        {page}
      </div>
    )
  }
}
export default Success;
